package com.stopstartgo

import android.app.Application
import com.stopstartgo.di.dataModule
import com.stopstartgo.di.domainModule
import com.stopstartgo.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class StartStopGoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@StartStopGoApplication)
            modules(dataModule, domainModule, viewModelsModule)
        }
    }
}