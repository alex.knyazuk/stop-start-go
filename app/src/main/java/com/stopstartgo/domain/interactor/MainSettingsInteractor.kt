package com.stopstartgo.domain.interactor

import com.stopstartgo.data.PrefsDataSource
import com.stopstartgo.data.model.Lang
import com.stopstartgo.data.model.MainBgType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainSettingsInteractor(
    private val prefsDataSource: PrefsDataSource
) {

    private val _isSoundEnabled = MutableStateFlow(prefsDataSource.isSoundEnabled())
    val isSoundEnabled = _isSoundEnabled.asStateFlow()

    private val _lang = MutableStateFlow(prefsDataSource.getSelectedLang())
    val lang = _lang.asStateFlow()

    private val _mainBgType = MutableStateFlow(prefsDataSource.getMainBg())
    val mainBgType = _mainBgType.asStateFlow()

    suspend fun updateSoundEnabled(enabled: Boolean) {
        _isSoundEnabled.emit(enabled)
        prefsDataSource.setSoundEnabled(enabled)
    }

    suspend fun updateSelectedLang(lang: Lang) {
        _lang.emit(lang)
        prefsDataSource.setSelectedLang(lang)
    }

    suspend fun updateSelectedBg(type: MainBgType) {
        _mainBgType.emit(type)
        prefsDataSource.setMainBg(type)
    }
}