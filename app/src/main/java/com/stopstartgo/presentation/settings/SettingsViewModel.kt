package com.stopstartgo.presentation.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.stopstartgo.data.PrefsDataSource
import com.stopstartgo.data.model.Lang
import com.stopstartgo.data.model.MainBgType
import com.stopstartgo.domain.interactor.MainSettingsInteractor
import kotlinx.coroutines.launch

class SettingsViewModel(
    private val prefsDataSource: PrefsDataSource,
    private val mainSettingsInteractor: MainSettingsInteractor,
) : ViewModel() {

    var isSoundEnabled: Boolean
        get() = mainSettingsInteractor.isSoundEnabled.value
        set(value) {
            viewModelScope.launch { mainSettingsInteractor.updateSoundEnabled(value) }
        }

    var isVibroEnabled: Boolean
        get() = prefsDataSource.isVibroEnabled()
        set(value) {
            prefsDataSource.setVibroEnabled(value)
        }

    private val _lang = MutableLiveData(prefsDataSource.getSelectedLang() ?: Lang.EN)
    val lang: LiveData<Lang> = _lang

    private val _mainBgType = MutableLiveData(prefsDataSource.getMainBg())
    val mainBgType: LiveData<MainBgType> = _mainBgType

    fun setSelectedLang(lang: Lang) {
        if (_lang.value != lang) {
            viewModelScope.launch { mainSettingsInteractor.updateSelectedLang(lang) }
            _lang.value = lang
        }
    }

    fun setSelectedBg(type: MainBgType) {
        if (_mainBgType.value != type) {
            viewModelScope.launch { mainSettingsInteractor.updateSelectedBg(type) }
            _mainBgType.value = type
        }
    }
}