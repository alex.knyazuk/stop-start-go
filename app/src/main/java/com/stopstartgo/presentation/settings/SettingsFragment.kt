package com.stopstartgo.presentation.settings

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.stopstartgo.R
import com.stopstartgo.data.model.Lang
import com.stopstartgo.data.model.MainBgType
import com.stopstartgo.databinding.FragmentSettingsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class SettingsFragment : Fragment(R.layout.fragment_settings) {

    private val viewModel by viewModel<SettingsViewModel>()
    private val binding by viewBinding(FragmentSettingsBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        observeLiveData()
    }

    private fun setupViews() = with(binding) {
        backBtn.setOnClickListener { findNavController().popBackStack() }

        soundCheck.setCheckedWithoutAnimation(viewModel.isSoundEnabled)
        soundCheck.setOnCheckedChangeListener { _, checked -> viewModel.isSoundEnabled = checked }

        vibroCheck.setCheckedWithoutAnimation(viewModel.isVibroEnabled)
        vibroCheck.setOnCheckedChangeListener { _, checked -> viewModel.isVibroEnabled = checked }

        imgLangEn.setOnClickListener { viewModel.setSelectedLang(Lang.EN) }
        imgLangRu.setOnClickListener { viewModel.setSelectedLang(Lang.RU) }

        imgBg1.setOnClickListener { viewModel.setSelectedBg(MainBgType.TYPE_1) }
        imgBg2.setOnClickListener { viewModel.setSelectedBg(MainBgType.TYPE_2) }
    }

    private fun observeLiveData() {
        viewModel.lang.observe(viewLifecycleOwner) { lang ->
            binding.imgLangEn.alpha = lang.toAlpha(Lang.EN)
            binding.imgLangRu.alpha = lang.toAlpha(Lang.RU)
        }
        viewModel.mainBgType.observe(viewLifecycleOwner) { type ->
            binding.imgBg1.isSelected = type == MainBgType.TYPE_1
            binding.imgBg2.isSelected = type == MainBgType.TYPE_2
        }
    }

    private fun CheckBox.setCheckedWithoutAnimation(checked: Boolean) {
        this.isChecked = checked
        this.jumpDrawablesToCurrentState()
    }

    private fun Lang.toAlpha(lang: Lang) = if (this == lang) 1f else 0.5f
}