package com.stopstartgo.presentation.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import java.util.*

val Configuration.localeCompat: Locale
    get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        locales.get(0)
    } else {
        @Suppress("DEPRECATION")
        locale
    }


fun Configuration.updateLocaleCompat(locale: Locale) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        setLocale(locale)
    } else {
        @Suppress("DEPRECATION")
        this.locale = locale
    }
}

fun Context.createWithNewResourcesLanguage(language: String): Context {
    val locale = Locale(language)
    Locale.setDefault(locale)
    val resources = this.resources
    val config = Configuration(resources.configuration)
    config.updateLocaleCompat(locale)
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        createConfigurationContext(config)
    } else {
        @Suppress("DEPRECATION")
        resources.updateConfiguration(config, resources.displayMetrics)
        this
    }
}
