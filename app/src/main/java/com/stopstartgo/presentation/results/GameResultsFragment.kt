package com.stopstartgo.presentation.results

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.stopstartgo.R
import com.stopstartgo.databinding.FragmentGameResultsBinding

class GameResultsFragment : Fragment(R.layout.fragment_game_results) {

    private val binding by viewBinding(FragmentGameResultsBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val score = requireArguments().getInt(KEY_SCORE)
        binding.gameOverScoreText.text = score.toString()

        binding.restartBtn.setOnClickListener {
            findNavController().navigate(R.id.action_gameResultsFragment_to_gameFragment)
        }
        binding.mainMenuBtn.setOnClickListener {
            findNavController().popBackStack(R.id.mainMenuFragment, inclusive = false)
        }
    }

    companion object {
        private const val KEY_SCORE = "SCORE"

        fun buildArgs(score: Int) = bundleOf(KEY_SCORE to score)
    }
}