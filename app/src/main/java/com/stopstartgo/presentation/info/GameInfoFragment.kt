package com.stopstartgo.presentation.info

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.stopstartgo.R
import com.stopstartgo.databinding.FragmentGameInfoBinding

class GameInfoFragment : Fragment(R.layout.fragment_game_info) {

    private val binding by viewBinding(FragmentGameInfoBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.setOnClickListener {
            findNavController().navigate(R.id.action_gameInfoFragment_to_gameFragment)
        }
    }
}