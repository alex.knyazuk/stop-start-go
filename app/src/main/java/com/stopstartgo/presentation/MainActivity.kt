package com.stopstartgo.presentation

import android.content.Context
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins
import androidx.core.view.updatePadding
import by.kirich1409.viewbindingdelegate.viewBinding
import com.stopstartgo.R
import com.stopstartgo.data.PrefsDataSource
import com.stopstartgo.data.model.Lang
import com.stopstartgo.data.model.MainBgType
import com.stopstartgo.databinding.ActivityMainBinding
import com.stopstartgo.presentation.utils.localeCompat
import com.stopstartgo.presentation.utils.createWithNewResourcesLanguage
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

class MainActivity : AppCompatActivity(), KoinComponent {

    private val viewModel by viewModel<MainActivityViewModel>()
    private val binding by viewBinding(ActivityMainBinding::bind)

    private val prefsDataSource by inject<PrefsDataSource>()

    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.statusBarColor = Color.TRANSPARENT
        setupWindowInsetsListener()

        mediaPlayer = MediaPlayer.create(this, R.raw.game_bg_music).apply { isLooping = true }

        resources.configuration.localeCompat.toLang()?.let { systemLang ->
            viewModel.updateDefaultLang(systemLang)
        }
        observeLiveData()
    }

    private fun setupWindowInsetsListener() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, windowInsets ->
            val statusBarHeight = windowInsets.getInsets(WindowInsetsCompat.Type.statusBars()).top
            val navigationBarHeight = windowInsets.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
            binding.fragmentContainer.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                updateMargins(top = statusBarHeight)
            }
            binding.root.updatePadding(bottom = navigationBarHeight)
            windowInsets
        }
    }

    private fun observeLiveData() {
        viewModel.isSoundEnabled.observe(this) { isSoundEnabled ->
            mediaPlayer?.let { player ->
                if (isSoundEnabled) {
                    player.start()
                } else {
                    player.pause()
                }
            }
        }
        viewModel.lang.observe(this) { lang ->
            val currentLang = resources.configuration.localeCompat.toLang()
            if (lang != null && lang != currentLang) {
                recreate()
            }
        }
        viewModel.mainBgType.observe(this) { type ->
            type ?: return@observe
            when (type) {
                MainBgType.TYPE_1 -> R.drawable.img_bg_1
                MainBgType.TYPE_2 -> R.drawable.img_bg_2
            }.let { binding.bgImage.setImageResource(it) }
        }
    }

    override fun attachBaseContext(newBase: Context) {
        val lang = prefsDataSource.getSelectedLang()
        Log.d("MainActivity", "attachBaseContext: lang=$lang")
        val newContext = lang?.code?.let { newBase.createWithNewResourcesLanguage(it) } ?: newBase
        super.attachBaseContext(newContext)
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer?.pause()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.isSoundEnabled.value == true) {
            mediaPlayer?.start()
        }
    }

    override fun onDestroy() {
        mediaPlayer?.release()
        mediaPlayer = null
        super.onDestroy()
    }

    private fun Locale.toLang(): Lang? = Lang.values().find { it.code == language }
}