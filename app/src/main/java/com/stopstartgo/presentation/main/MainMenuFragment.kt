package com.stopstartgo.presentation.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.stopstartgo.R
import com.stopstartgo.databinding.FragmentMainMenuBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainMenuFragment : Fragment(R.layout.fragment_main_menu) {

    private val viewModel by viewModel<MainMenuViewModel>()
    private val binding by viewBinding(FragmentMainMenuBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.startGameBtn.setOnClickListener {
            if (viewModel.getAndSetInfoShown()) {
                findNavController().navigate(R.id.action_mainMenuFragment_to_gameFragment)
            } else {
                findNavController().navigate(R.id.action_mainMenuFragment_to_gameInfoFragment)
            }
        }
        binding.settingsBtn.setOnClickListener {
            findNavController().navigate(R.id.action_mainMenuFragment_to_settingsFragment)
        }
    }
}