package com.stopstartgo.presentation.main

import androidx.lifecycle.ViewModel
import com.stopstartgo.data.PrefsDataSource

class MainMenuViewModel(
    private val prefsDataSource: PrefsDataSource
) : ViewModel() {

    fun getAndSetInfoShown(): Boolean {
        return prefsDataSource.isInfoShown().also {
            prefsDataSource.setInfoShown(true)
        }
    }
}