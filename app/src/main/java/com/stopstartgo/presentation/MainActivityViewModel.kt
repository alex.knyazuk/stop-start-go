package com.stopstartgo.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.stopstartgo.data.PrefsDataSource
import com.stopstartgo.data.model.Lang
import com.stopstartgo.domain.interactor.MainSettingsInteractor

class MainActivityViewModel(
    private val mainSettingsInteractor: MainSettingsInteractor,
    private val prefsDataSource: PrefsDataSource
) : ViewModel() {

    val lang = mainSettingsInteractor.lang.asLiveData()
    val mainBgType = mainSettingsInteractor.mainBgType.asLiveData()
    val isSoundEnabled = mainSettingsInteractor.isSoundEnabled.asLiveData()

    fun updateDefaultLang(defaultLang: Lang) {
        if (mainSettingsInteractor.lang.value == null) {
            prefsDataSource.setSelectedLang(defaultLang)
        }
    }
}