package com.stopstartgo.presentation.game.model

enum class GameVibroEffect {
    CLICK, FINISH
}