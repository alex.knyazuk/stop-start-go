package com.stopstartgo.presentation.game

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.stopstartgo.R
import com.stopstartgo.databinding.FragmentGameBinding
import com.stopstartgo.presentation.game.model.GameStateUI
import com.stopstartgo.presentation.game.model.GameVibroEffect
import com.stopstartgo.presentation.results.GameResultsFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class GameFragment : Fragment(R.layout.fragment_game) {

    private val viewModel by viewModel<GameViewModel>()
    private val binding by viewBinding(FragmentGameBinding::bind)

    private val vibrator by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            (requireContext().getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager).defaultVibrator
        } else {
            @Suppress("DEPRECATION")
            requireContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        observeLiveData()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupViews() {
        binding.gameActionBtn.setOnClickListener { viewModel.onGameBtnClicked() }
        binding.gameActionBtn.setOnTouchListener { v, event ->
            return@setOnTouchListener when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.performClick()
                    updateGameButton(pressed = true)
                    true
                }
                MotionEvent.ACTION_UP -> {
                    updateGameButton(pressed = false)
                    true
                }
                else -> false
            }
        }
    }

    private fun observeLiveData() {
        viewModel.score.observe(viewLifecycleOwner) { score ->
            binding.scoreText.text = score.toString()
        }
        viewModel.trafficLightsState.observe(viewLifecycleOwner) { state ->
            when (state) {
                GameStateUI.STOP -> binding.trafficLightsImg.setImageResource(R.drawable.ic_traffic_lights_stop)
                GameStateUI.START -> binding.trafficLightsImg.setImageResource(R.drawable.ic_traffic_lights_start)
                GameStateUI.GO -> binding.trafficLightsImg.setImageResource(R.drawable.ic_traffic_lights_go)
                GameStateUI.NONE -> binding.trafficLightsImg.setImageResource(R.drawable.ic_traffic_lights_none)
                else -> {} // do nothing
            }
        }
        viewModel.gameBtnState.observe(viewLifecycleOwner) { state ->
            binding.gameActionBtnText.isVisible = state != null && state != GameStateUI.NONE
            when (state) {
                GameStateUI.STOP -> binding.gameActionBtnText.setText(R.string.game_stop)
                GameStateUI.START -> binding.gameActionBtnText.setText(R.string.game_start)
                GameStateUI.GO -> binding.gameActionBtnText.setText(R.string.game_go)
                else -> {} // do nothing
            }
        }
        viewModel.gameVibroEffect.observe(viewLifecycleOwner) { effect ->
            effect ?: return@observe
            vibrator.cancel()
            when (effect) {
                GameVibroEffect.CLICK -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
                } else {
                    @Suppress("DEPRECATION")
                    vibrator.vibrate(200)
                }
                GameVibroEffect.FINISH -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE))
                } else {
                    @Suppress("DEPRECATION")
                    vibrator.vibrate(1000)
                }
            }
        }
        viewModel.gameFinished.observe(viewLifecycleOwner) { gameFinished ->
            if (gameFinished) {
                findNavController().navigate(
                    R.id.action_gameFragment_to_gameResultsFragment,
                    GameResultsFragment.buildArgs(viewModel.score.value ?: 0)
                )
            }
        }
    }

    private fun updateGameButton(pressed: Boolean) {
        binding.gameActionBtn.setImageResource(
            if (pressed) R.drawable.ic_btn_blue_pressed else R.drawable.ic_btn_blue
        )
        binding.gameActionBtnText.updateLayoutParams<ViewGroup.MarginLayoutParams> {
            val marginRes = if (pressed) R.dimen.game_btn_text_margin_pressed else R.dimen.game_btn_text_margin
            updateMargins(bottom = requireContext().resources.getDimensionPixelSize(marginRes))
        }
    }
}