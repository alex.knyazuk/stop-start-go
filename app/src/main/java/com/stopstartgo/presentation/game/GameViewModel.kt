package com.stopstartgo.presentation.game

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.stopstartgo.data.PrefsDataSource
import com.stopstartgo.presentation.game.model.GameStateUI
import com.stopstartgo.presentation.game.model.GameVibroEffect
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random

class GameViewModel(
    prefsDataSource: PrefsDataSource
) : ViewModel() {

    private val isVibroEnabled = prefsDataSource.isVibroEnabled()

    private val _score = MutableLiveData(0)
    val score: LiveData<Int> = _score

    private val _trafficLightsState = MutableLiveData(GameStateUI.NONE)
    val trafficLightsState: LiveData<GameStateUI> = _trafficLightsState

    private val _gameBtnState = MutableLiveData(GameStateUI.NONE)
    val gameBtnState: LiveData<GameStateUI> = _gameBtnState

    private val _gameVibroEffect = MutableLiveData<GameVibroEffect?>(null)
    val gameVibroEffect: LiveData<GameVibroEffect?> = _gameVibroEffect

    private val _gameFinished = MutableLiveData(false)
    val gameFinished: LiveData<Boolean> = _gameFinished

    private var lastClickedState: GameStateUI? = null

    init {
        viewModelScope.launch {
            delay(2000)
            launch {
                while (_gameFinished.value == false) {
                    val newStates = GameStateUI.gameStates.filter { it != _trafficLightsState.value }
                    _trafficLightsState.value = newStates[Random.nextInt(0, newStates.size)]
                    randomGameDelay()
                }
            }

            launch {
                while (_gameFinished.value == false) {
                    val newStates = GameStateUI.gameStates.filter { it != _gameBtnState.value }
                    _gameBtnState.value = newStates[Random.nextInt(0, newStates.size)]
                    lastClickedState = null
                    randomGameDelay()
                }
            }
        }
    }

    private suspend fun randomGameDelay() {
        val delay = Random.nextInt(1, 6)
        delay(delay * 500L)
    }

    fun onGameBtnClicked() {
        val trafficLightsState = _trafficLightsState.value
        val gameBtnState = _gameBtnState.value
        if ((lastClickedState == null || lastClickedState != gameBtnState) && trafficLightsState != GameStateUI.NONE) {
            lastClickedState = gameBtnState
            if (gameBtnState == trafficLightsState) {
                _score.value = (_score.value ?: 0) + SCORE_ADD
                makeClickVibro()
            } else {
                makeFinishVibro()
                _gameFinished.value = true
            }
        }
    }

    private fun makeClickVibro() {
        if (isVibroEnabled) {
            _gameVibroEffect.value = GameVibroEffect.CLICK
            _gameVibroEffect.value = null
        }
    }

    private fun makeFinishVibro() {
        if (isVibroEnabled) {
            _gameVibroEffect.value = GameVibroEffect.FINISH
            _gameVibroEffect.value = null
        }
    }

    companion object {
        private const val SCORE_ADD = 3
    }
}