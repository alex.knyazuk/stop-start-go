package com.stopstartgo.presentation.game.model

enum class GameStateUI {
    STOP,
    START,
    GO,

    NONE;
    companion object {
        val gameStates = listOf(STOP, START, GO)
    }
}