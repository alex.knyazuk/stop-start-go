package com.stopstartgo.data

import android.content.Context
import android.content.SharedPreferences
import com.stopstartgo.data.model.Lang
import com.stopstartgo.data.model.MainBgType

interface PrefsDataSource {
    fun isInfoShown(): Boolean
    fun setInfoShown(shown: Boolean)

    fun isSoundEnabled(): Boolean
    fun setSoundEnabled(enabled: Boolean)

    fun isVibroEnabled(): Boolean
    fun setVibroEnabled(enabled: Boolean)

    fun getSelectedLang(): Lang?
    fun setSelectedLang(lang: Lang)

    fun getMainBg(): MainBgType
    fun setMainBg(mainBgType: MainBgType)
}

class PrefsDataSourceImpl(
    context: Context
) : PrefsDataSource {

    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    override fun isInfoShown(): Boolean {
        return prefs.getBoolean(KEY_INFO_SHOWN, false)
    }

    override fun setInfoShown(shown: Boolean) {
        prefs.edit().putBoolean(KEY_INFO_SHOWN, shown).apply()
    }

    override fun isSoundEnabled(): Boolean {
        return prefs.getBoolean(KEY_SOUND_ENABLED, true)
    }

    override fun setSoundEnabled(enabled: Boolean) {
        prefs.edit().putBoolean(KEY_SOUND_ENABLED, enabled).apply()
    }

    override fun isVibroEnabled(): Boolean {
        return prefs.getBoolean(KEY_VIBRO_ENABLED, true)
    }

    override fun setVibroEnabled(enabled: Boolean) {
        prefs.edit().putBoolean(KEY_VIBRO_ENABLED, enabled).apply()
    }

    override fun getSelectedLang(): Lang? {
        val langString = prefs.getString(KEY_SELECTED_LANG, null)
        return langString?.let { Lang.valueOf(it) }
    }

    override fun setSelectedLang(lang: Lang) {
        prefs.edit().putString(KEY_SELECTED_LANG, lang.name).apply()
    }

    override fun getMainBg(): MainBgType {
        return prefs.getString(KEY_SELECTED_BG, null)?.let { MainBgType.valueOf(it) } ?: MainBgType.TYPE_1
    }

    override fun setMainBg(mainBgType: MainBgType) {
        prefs.edit().putString(KEY_SELECTED_BG, mainBgType.name).apply()
    }

    companion object {
        const val PREFS_NAME = "app_prefs"
        private const val KEY_INFO_SHOWN = "INFO_SHOWN"
        private const val KEY_SOUND_ENABLED = "SOUND_ENABLED"
        private const val KEY_VIBRO_ENABLED = "VIBRO_ENABLED"
        const val KEY_SELECTED_LANG = "SELECTED_LANG"
        private const val KEY_SELECTED_BG = "SELECTED_BG"
    }
}