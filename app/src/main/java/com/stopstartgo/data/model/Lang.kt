package com.stopstartgo.data.model

enum class Lang(val code: String) {
    EN("en"), RU("ru")
}