package com.stopstartgo.di

import com.stopstartgo.domain.interactor.MainSettingsInteractor
import org.koin.dsl.module

val domainModule = module {
    single { MainSettingsInteractor(get()) }
}