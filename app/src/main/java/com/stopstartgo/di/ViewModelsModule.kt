package com.stopstartgo.di

import com.stopstartgo.presentation.MainActivityViewModel
import com.stopstartgo.presentation.game.GameViewModel
import com.stopstartgo.presentation.main.MainMenuViewModel
import com.stopstartgo.presentation.settings.SettingsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    viewModel { MainActivityViewModel(get(), get())  }
    viewModel { MainMenuViewModel(get())  }
    viewModel { SettingsViewModel(get(), get())  }
    viewModel { GameViewModel(get())  }
}