package com.stopstartgo.di

import com.stopstartgo.data.PrefsDataSource
import com.stopstartgo.data.PrefsDataSourceImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single<PrefsDataSource> { PrefsDataSourceImpl(androidContext()) }
}